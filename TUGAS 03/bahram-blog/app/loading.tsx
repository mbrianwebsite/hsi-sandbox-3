import LoadingInfo from "./components/LoadingInfo";

export default function Loading() {
  // You can add any UI inside Loading, including a Skeleton.
  return <LoadingInfo />;
}
