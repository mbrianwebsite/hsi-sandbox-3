import axios from "axios";
import BlogCard from "./components/BlogCard";
import Link from "next/link";
import MoreBlog from "./components/MoreBlog";

const fetchArticles = async (sortKey: string) => {
  const fetchData = await axios
    .get(
      `https://hsi-sandbox.vercel.app/api/articles?sort=${sortKey == null ? "new" : sortKey}`,
    )
    .then(function (response) {
      return response.data.data;
    });
  return fetchData;
};

export default async function Home({
  searchParams,
}: {
  searchParams: { sort: string };
}) {
  const blog = await fetchArticles(searchParams.sort);

  return (
    <main className="mx-auto mb-24 flex max-w-7xl flex-col px-4 md:px-8">
      <div className="mx-auto flex max-w-5xl flex-col gap-12 md:gap-24">
        {blog &&
          blog.map((b: any, index: string) => {
            return (
              <Link href={b.slug} key={index}>
                <BlogCard
                  thumbnail={b.thumbnail}
                  title={b.title}
                  slug={b.slug}
                  category={b.category.name}
                  author={b.author.firstName + " " + b.author.lastName}
                />
              </Link>
            );
          })}
        <MoreBlog />
      </div>
    </main>
  );
}
