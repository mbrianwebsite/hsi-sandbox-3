export default function Button({
  title,
  onClick,
  state,
}: {
  title: string;
  onClick?: () => void;
  state: boolean;
}) {
  return (
    <div
      className={`mx-auto w-full cursor-pointer rounded-full px-5 py-3 text-center text-lg font-semibold -tracking-tight md:w-fit md:px-10 md:py-5 md:text-2xl md:hover:scale-125 md:hover:shadow-2xl md:hover:shadow-button ${state ? "text-button ring ring-button" : "bg-button text-white"}`}
      onClick={onClick}
    >
      {title}
    </div>
  );
}
