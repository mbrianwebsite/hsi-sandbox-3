import Button from "./Button";

export default function LoadingInfo() {
  return <Button title="Loading..." state={true} />;
}
