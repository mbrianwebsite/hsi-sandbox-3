import Link from "next/link";
import BlogInfo from "./BlogInfo";

export default function RelatedListCard({
  thumbnail,
  title,
  slug,
  category,
  author,
  summary,
  number,
}: {
  thumbnail: string;
  title: string;
  slug: string;
  category: string;
  author: string;
  summary: string;
  number: number;
}) {
  return (
    <Link href={`/${slug}`}>
      <div className="flex flex-row-reverse gap-5 rounded-lg bg-white drop-shadow-sm">
        <img
          src={thumbnail}
          className="max-h-[360px] w-[641px] rounded-r-md object-cover"
        />
        <div className="flex flex-col gap-5 p-10">
          <div className="text-2xl">{`${(number + 1).toString().padStart(2, "0")}`}</div>
          <BlogInfo author={author} category={category} />
          <h2 className="text-xl font-semibold capitalize leading-[46px] -tracking-wide text-blogTitle md:text-4xl">
            {title}
          </h2>
          <div className="text-lg text-textInfo">{summary}</div>
        </div>
      </div>
    </Link>
  );
}
