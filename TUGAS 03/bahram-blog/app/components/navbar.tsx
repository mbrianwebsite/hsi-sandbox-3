"use client";

import Link from "next/link";
import NavLink from "./NavLink";
import { usePathname } from "next/navigation";

export default function Navbar() {
  const path = usePathname();
  return (
    <div className="mx-auto flex max-w-7xl flex-row-reverse items-center justify-between px-4 py-7 md:flex-row md:justify-normal md:px-8 md:py-14">
      <div className="flex  flex-row items-center justify-end gap-4 md:w-1/3 md:justify-start md:gap-9">
        {path == "/" ? (
          <>
            <NavLink title="Popular" href="/?sort=popular" />
            <NavLink title="New" href="/?sort=new" />
          </>
        ) : (
          ""
        )}
      </div>
      <Link
        href={"/"}
        className="flex cursor-pointer flex-row items-center justify-center md:w-1/3"
      >
        <img src="/bahram-logo.png" alt="" className="center h-fit w-fit " />
      </Link>
      <div className="hidden flex-row items-center justify-end md:flex md:w-1/3">
        {path == "/" ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="h-6 w-6 cursor-pointer md:hidden"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
            />
          </svg>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
