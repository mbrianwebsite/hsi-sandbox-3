import Link from "next/link";
import BlogInfo from "./BlogInfo";

export default function RelatedCard({
  thumbnail,
  title,
  slug,
  category,
  author,
  summary,
}: {
  thumbnail: string;
  title: string;
  slug: string;
  category: string;
  author: string;
  summary: string;
}) {
  return (
    <Link href={`/${slug}`}>
      <div className="flex flex-col gap-5">
        <img src={thumbnail} className="rounded" />
        <div className="flex flex-col gap-5">
          <BlogInfo author={author} category={category} />
          <h2 className="text-xl font-semibold capitalize leading-[46px] -tracking-wide text-blogTitle md:text-4xl">
            {title}
          </h2>
          <div className="text-lg text-textInfo">{summary}</div>
        </div>
      </div>
    </Link>
  );
}
