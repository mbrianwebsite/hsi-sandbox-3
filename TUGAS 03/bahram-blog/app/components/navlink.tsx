"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import { useSearchParams } from "next/navigation";

export default function NavLink({
  title,
  href,
}: {
  title: string;
  href: string;
}) {
  const path = usePathname();
  const searchParams = useSearchParams();
  return (
    <Link
      href={href}
      className={
        "cursor-pointer rounded-lg px-5 py-2 text-base font-semibold " +
        (path +
          "?sort=" +
          (searchParams.get("sort") == null
            ? "new"
            : searchParams.get("sort")) ==
          href && "bg-button text-white")
      }
    >
      {title}
    </Link>
  );
}
