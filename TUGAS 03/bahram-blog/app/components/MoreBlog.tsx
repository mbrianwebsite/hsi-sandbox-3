"use client";

import Link from "next/link";
import BlogCard from "./BlogCard";
import { Suspense, useEffect, useState } from "react";
import axios from "axios";
import Button from "./Button";
import { useSearchParams } from "next/navigation";
import Loading from "../loading";

export default function MoreBlog() {
  const [blog, setBlog] = useState([] as any[]);

  const [loading, setLoading] = useState(false);

  const [button, setButton] = useState(true);

  const [apiPage, setApiPage] = useState(2);

  const searchParams = useSearchParams();

  const fetchArticles = async (sortKey: string) => {
    setLoading(true);
    const fetchData = await axios
      .get(
        `https://hsi-sandbox.vercel.app/api/articles?sort=${sortKey == "null" ? "new" : sortKey}&page=${apiPage}`,
      )
      .then(function (response) {
        let currentPage = response.data.meta.pagination.page;
        let totalPage = response.data.meta.pagination.totalPages;
        let newData = response.data.data;
        setBlog([...blog, ...newData]);
        setApiPage(currentPage + 1);
        if (currentPage == totalPage) {
          setButton(false);
        }
      });

    setLoading(false);
    return fetchData;
  };

  const loadMore = async (sortKey: string) => {
    fetchArticles(sortKey);
  };

  useEffect(() => {
    setButton(true);
    setBlog([]);
    setApiPage(2);
  }, [searchParams.get("sort")]);
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {blog &&
            blog.map((b, index) => {
              return (
                <Link href={b.slug} key={index}>
                  <BlogCard
                    thumbnail={b.thumbnail}
                    title={b.title}
                    slug={b.slug}
                    category={b.category.name}
                    author={b.author.firstName + " " + b.author.lastName}
                  />
                </Link>
              );
            })}
          {button && (
            <Button
              title={button ? "Load More" : "End of Blog"}
              onClick={() => loadMore(`${searchParams.get("sort")}`)}
              state={button}
            />
          )}
        </>
      )}
    </>
  );
}
