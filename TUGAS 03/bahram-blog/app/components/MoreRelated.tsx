"use client";

import { useState } from "react";
import axios from "axios";
import Button from "./Button";
import RelatedListCard from "./RelatedListCard";
import Loading from "../loading";

export default function MoreRelated({
  categoryId,
  excludeId,
  perPage,
}: {
  categoryId: number;
  excludeId: number;
  perPage: number;
}) {
  const [article, setArticle] = useState([] as any[]);

  const [button, setButton] = useState(true);

  const [apiPage, setApiPage] = useState(2);

  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    const fetchData = await axios
      .get(
        `https://hsi-sandbox.vercel.app/api/articles?categoryId=${categoryId}&perPage=${perPage}&excludedArticleId=${excludeId}&page=${apiPage}`,
      )
      .then(function (response) {
        let currentPage = response.data.meta.pagination.page;
        let totalPage = response.data.meta.pagination.totalPages;
        let newData = response.data.data;
        setArticle([...article, ...newData]);
        if (currentPage >= totalPage) {
          setButton(false);
        }
        setApiPage(currentPage + 1);
      });
    setLoading(false);
    return fetchData;
  };

  const loadMore = async () => {
    fetchArticles();
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {article &&
            article.map((a, index) => {
              return (
                <RelatedListCard
                  key={index}
                  number={index + perPage}
                  thumbnail={a.thumbnail}
                  title={a.title}
                  slug={a.slug}
                  category={a.category.name}
                  author={a.author.firstName + " " + a.author.lastName}
                  summary={a.summary}
                />
              );
            })}
          {button && (
            <Button
              title={button ? "Load More" : "End of Blog"}
              onClick={() => loadMore()}
              state={button}
            />
          )}
        </>
      )}
    </>
  );
}
