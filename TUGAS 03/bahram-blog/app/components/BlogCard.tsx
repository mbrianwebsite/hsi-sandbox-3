import BlogInfo from "./BlogInfo";

export default function BlogCard({
  thumbnail,
  title,
  slug,
  category,
  author,
}: {
  thumbnail: string;
  title: string;
  slug: string;
  category: string;
  author: string;
}) {
  return (
    <div className="flex flex-col gap-5">
      <img src={thumbnail} className="rounded" />
      <div className="flex flex-col gap-2">
        <BlogInfo author={author} category={category} />
        <h2 className="text-2xl font-semibold capitalize leading-normal tracking-wide text-blogTitle md:text-5xl">
          {title}
        </h2>
      </div>
    </div>
  );
}
