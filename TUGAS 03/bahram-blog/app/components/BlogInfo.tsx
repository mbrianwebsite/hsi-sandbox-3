export default function BlogInfo({
  author,
  category,
}: {
  author: string;
  category: string;
}) {
  return (
    <div className="break-all uppercase">
      <span className="text-textInfo">by</span>&nbsp;&nbsp;
      <span>{author}</span>&nbsp;&nbsp;
      <span className="text-textInfo">in</span>&nbsp;&nbsp;
      <span>{category}</span>
    </div>
  );
}
