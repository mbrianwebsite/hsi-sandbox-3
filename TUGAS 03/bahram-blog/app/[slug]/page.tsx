import axios from "axios";
import BlogInfo from "../components/BlogInfo";
import RelatedCard from "../components/RelatedCard";

const fetchArticle = async (slug: string) => {
  const fetchData = await axios
    .get(`https://hsi-sandbox.vercel.app/api/articles/${slug}`)
    .then(function (response) {
      return response.data.data;
    });
  return fetchData;
};

const fetchRelatedArticles = async (
  slug: string,
  categoryId: number,
  excludeId: number,
) => {
  const fetchData = await axios
    .get(
      `https://hsi-sandbox.vercel.app/api/articles?categoryId=${categoryId}&perPage=2&excludedArticleId=${excludeId}`,
    )
    .then(function (response) {
      return response.data.data;
    });
  return fetchData;
};

export default async function DetailPage({
  params,
}: {
  params: { slug: string };
}) {
  const article = await fetchArticle(params.slug);
  const relatedArticles = await fetchRelatedArticles(
    params.slug,
    article.category.id,
    article.id,
  );

  return (
    <main className="bg-background">
      {article && (
        <>
          <div className="bg-white">
            <div className="mx-auto flex max-w-7xl flex-col gap-7 px-4 py-8 md:px-8 md:py-16">
              <div className="text-4xl font-bold text-textBody">
                {article.title}
              </div>
              <div className="text-lg font-semibold">{article.summary}</div>
              <BlogInfo
                author={`${article.author.firstName} ${article.author.lastName}`}
                category={`${article.category.name}`}
              />
            </div>
          </div>
          <div className="mx-auto flex max-w-7xl flex-col gap-12 px-4 py-8 md:px-8 md:py-16">
            <img src={article.thumbnail} className="rounded" />
            <div className="text-xl leading-10 text-textBody">
              {article.content}
            </div>
          </div>
        </>
      )}

      <div className="mx-auto flex max-w-7xl flex-col gap-12 px-4 py-8 md:px-8 md:py-16">
        {relatedArticles.length > 0 && (
          <div className="flex flex-row items-center justify-between">
            <div className=" text-2xl font-semibold text-blogTitle md:text-4xl">
              You might also like...
            </div>
            <a
              href={`/${article.slug}/relates`}
              className="hover:drop-shadow-black cursor-pointer text-textInfo hover:scale-125 hover:font-semibold hover:drop-shadow-md"
            >
              More
            </a>
          </div>
        )}
        <div className="grid grid-cols-1 gap-16 md:grid-cols-2 md:gap-7">
          {relatedArticles.map((relatiedArticle: any, index: any) => {
            return (
              <RelatedCard
                key={index}
                thumbnail={relatiedArticle.thumbnail}
                title={relatiedArticle.title}
                slug={relatiedArticle.slug}
                category={relatiedArticle.category.name}
                author={
                  relatiedArticle.author.firstName +
                  " " +
                  relatiedArticle.author.lastName
                }
                summary={article.summary}
              />
            );
          })}
        </div>
      </div>
    </main>
  );
}
