import MoreRelated from "@/app/components/MoreRelated";
import RelatedListCard from "@/app/components/RelatedListCard";
import axios from "axios";

const perPage = 4;

const fetchArticle = async (slug: string) => {
  const fetchData = await axios
    .get(`https://hsi-sandbox.vercel.app/api/articles/${slug}`)
    .then(function (response) {
      return response.data.data;
    });
  return fetchData;
};

const fetchRelatedArticles = async (
  slug: string,
  categoryId: number,
  excludeId: number,
) => {
  const fetchData = await axios
    .get(
      `https://hsi-sandbox.vercel.app/api/articles?categoryId=${categoryId}&perPage=${perPage}&excludedArticleId=${excludeId}`,
    )
    .then(function (response) {
      return response.data.data;
    });
  return fetchData;
};

export default async function Relates({
  params,
}: {
  params: { slug: string };
}) {
  const article = await fetchArticle(params.slug);
  const relatiedArticles = await fetchRelatedArticles(
    params.slug,
    article.category.id,
    article.id,
  );

  return (
    <main className="bg-background">
      {article && (
        <div className="bg-white">
          <div className="mx-auto flex max-w-7xl flex-col gap-9  py-8">
            <div className=" px-4 text-2xl font-bold md:px-8 md:text-4xl">
              Related Post List
            </div>
            <div className="mx-auto flex max-w-7xl flex-col gap-6 px-4 pb-8 md:flex-row md:gap-12 md:px-8 md:pb-16">
              <img
                src={article.thumbnail}
                className="h-40 w-full rounded-md object-cover object-right md:h-56 md:w-48"
              ></img>
              <div className="flex flex-col gap-7">
                <div className="text-4xl font-bold text-textBody">
                  {article.title}
                </div>
                <div className="text-lg font-semibold">{article.summary}</div>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="mx-auto flex max-w-7xl flex-col gap-12 px-4 py-8 md:px-8 md:py-16">
        <div className="flex flex-col gap-16">
          {relatiedArticles.map((relatiedArticle: any, index: any) => {
            return (
              <RelatedListCard
                key={index}
                number={index}
                thumbnail={relatiedArticle.thumbnail}
                title={relatiedArticle.title}
                slug={relatiedArticle.slug}
                category={relatiedArticle.category.name}
                author={
                  relatiedArticle.author.firstName +
                  " " +
                  relatiedArticle.author.lastName
                }
                summary={article.summary}
              />
            );
          })}
          <MoreRelated
            perPage={perPage}
            categoryId={article.category.id}
            excludeId={article.id}
          />
        </div>
      </div>
    </main>
  );
}
