"use client";

import { useRouter } from "next/navigation";
import Button from "./components/Button";

export default function Error({ error }: { error: Error }) {
  const router = useRouter();
  const backToHome = () => {
    router.push("/");
  };
  return (
    <div className="flex w-full items-center justify-center px-4 md:px-12">
      <div className="flex w-full flex-col items-center justify-center rounded-lg border bg-white px-4 py-4 shadow-2xl">
        <p className="text-6xl font-bold tracking-wider md:text-7xl lg:text-9xl">
          404
        </p>
        <p className="text-brand-primary-600 mt-4 text-2xl font-bold tracking-wider md:text-3xl lg:text-5xl">
          Article not found.
        </p>
        <p className="mt-4 border-b-2 pb-4 text-center">{error.message}</p>
        <div className="py-8">
          <Button title="Back to Home" state={true} onClick={backToHome} />
        </div>
      </div>
    </div>
  );
}
